SELECT `code` AS `Код заказа` ,`desc` AS `Статус`,`date` AS `Дата`,`time` AS `Время` 
FROM (
	SELECT `order`.`code`, `order_status`.`desc`, `order_status_history`.`status_id`, `order_status_history`.`date`, `order_status_history`.`time` 
	FROM `order`
	JOIN `order_status_history`  ON (`order`.`id` = `order_status_history`.`order_id`) 
	JOIN `order_status` ON (`order_status`.`id` =`order_status_history`.`status_id`)
	ORDER BY `date`,`time`
	)`query` 
GROUP BY `code`
;

